# Created by nshim at 4/24/2019
Feature: Validate that non existing user can't get into the privileged dashboard
  In order to manage content
  As a developer
  I want to ensure that a non existent user doesn't have access
  to the main dashboard so that they can't make any changes

# Scenario: That validates that there's an admin button in the footer
#           When the button is clicked the user is taken to the login page
  Scenario: Confirm navigation to user login
    Then I click on the user icon at the bottom right of the footer
    Then I validate that the current link is "http://localhost:8000/priv/login/"
    Then I enter "mike" as my username
    Then I enter "testingsite" as my password
    Then I click on the login
    And I exit the webapp