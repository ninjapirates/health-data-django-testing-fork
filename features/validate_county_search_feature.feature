# Created by nshim at 4/24/2019
Feature: County Search Validation
  In order to view a county's ranking
  As a user of CPH-IVT
  I want to be able to search for a county using the search feature
  Background: I am on the public page
    Given I am on public user home page
    When  I go to the "/" page

# Scenario: that validates the navigation by entering and searching
#           for a county then returning a a page that contains that
#           county. This county will hold the different Health states if any
  Scenario: Confirm that a specified county is searched for
    Then I enter "Washington County TN" in the search field
    Then I click the search button
    Then I get a page that display "Washington County" as a county in Tennessee
    And I exit the webapp