"""
This file will be used to setup the functions required to run the tests
The code was adopted from https://github.com/Rhoynar/python-selenium-bdd

Modified by: Jean-Marie

"""
# Selenium is required for the webdriver to function
from selenium import webdriver
from features.test_config.config import settings
from urllib.parse import urljoin
import time

class Setup:
	instance = None

	@classmethod
	def get_instance(cls):
		if cls.instance is None:
			cls.instance = Setup()
		return cls.instance

	def __init__(self):
		if str(settings['browser']).lower() is "firefox":
			self.driver = webdriver.Firefox()
		elif str(settings['browser']).lower() is "chrome":
			self.driver = webdriver.Chrome()
		else:
			self.driver = webdriver.Firefox()

	def get_driver(self):
		'''
		:return: returns the webdriver - chrome of firefox
		'''
		return self.driver

	def load_website(self):
		'''
		:return: A specified page
		'''
		self.driver.get(settings['url'])

	# Close the browser
	def teardown(self):
		time.sleep(5)
		self.driver.close()

	def goto_page(self, page):
		'''
		:param page: the url to the page being tested
		:return: returns the page the QA specifies
		'''
		self.driver.get(urljoin(settings['url'], page.lower()))

	def verify_component_exists(self, component):
		'''
		:param component: is the item the QA will be searching for on a page such as a button or text
		:return: True if the item is found else the test fails
		'''
		# Simple implementation
		assert component in self.driver.find_element_by_tag_name('body').text, \
        "Component {} not found on page".format(component)

	def verify_component_button(self, component):
		assert component in self.driver.find_elements_by_link_text(component)

	def input_text_to_form(self, inputText_id):
		"""
		The returned xpath will be filled with the user's text
		:param inputText_id:
		:return: Return the xpath of a given form filled
		"""
		return self.driver.find_element_by_id(inputText_id)


	def click_component_by_xpath(self, component):
		"""
		click on a button given the button's xpath
		:param component:
		:return:
		"""
		self.driver.find_element_by_xpath(component).click()

	def verify_element_by_class_name(self, component):
		self.driver.find_element_by_class_name(component).click()

	def check_element_existance(self, component):
		assert self.driver.find_elements_by_css_selector(component)

	def find_the_current_url(self):
		'''
		:return: current_url as localhost:xxxx
		'''
		return self.driver.current_url

cph_ivt_app = Setup.get_instance()
