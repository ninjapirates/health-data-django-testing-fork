# Created by Jean-Marie at 2/21/2019
Feature: Open CPH IVT
  In order to visualize county ranking
  As a user of CPH-IVT
  I want to be able to get on the home page

  Background: I am on the public page
    Given I am on public user home page
    When  I go to the "/" page


# Scenario: that validates that there is a home button. When the button is
#           the button is clicked it keeps the user on the same page
  Scenario: Confirm that a home button is present on the navigation bar
    Then I click on the home button
    And I confirm that I am still on the home page

  Scenario: Confirm that Find a Location is on the page
    Then I see a find county button with a link "Find a county"
    Then I see a find sate button with a link "Find a state"
    And  I see content "Find a Location"



## Scenario: That validates that there's an admin button in the footer
##           When the button is clicked the user is taken to the login page
#  Scenario: Confirm navigation to user login
#    Then I click on the user icon at the bottom right of the footer
#    Then I validate that the current link is "http://localhost:8000/priv/login/"
#    Then I enter "admin" as my username
#    Then I enter "testing" as my password
#    Then I click on the login
#    And I exit the webapp
