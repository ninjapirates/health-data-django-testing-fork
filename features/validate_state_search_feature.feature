# Created by nshim at 4/24/2019
Feature: State Search Validation
  In order to view all county rankings
  As a user of CPH-IVT
  I want to be able to search for a state using the search feature
  Background: I am on the public page
    Given I am on public user home page
    When  I go to the "/" page

# Scenario: that validates the navigation by entering and searching
#           for a state then returning a page that contains that
#           state. This state will hold ranking of all counties in a states.
 Scenario: Confirm that a specified state Exist in the database
    Then I enter "Tennessee" in the search field
    Then I click the search button
    Then I get a page that display "Tennessee" as a state
    And I exit the webapp