Feature: Admin Login Validation
  In order to carry out my managment tasks
  As a user of CPH-IVT
  I want to be able to log in to the privileged user dashboard
  Background: I am on the login page
    Given I am on the login page
    When  I go to the login page "http://localhost:8000/priv/login/"

# Scenario: Validates that admin page log in works
#            When user clicks button to submit
#           redirects user to privileged user main page

  Scenario: Confirm cancel button on admin page works
    Then I enter "admin" as my username
    Then I enter "testing" as my password
    Then I click on the cancel button
    Then I validate that the current link is "http://localhost:8000/"



  Scenario: Confirm submit button on admin page works
    Then I enter "admin" as my username
    Then I enter "testing" as my password
    Then I click on the login
    Then I validate that the current link is "http://localhost:8000/priv/home/"
    And I exit the webapp
