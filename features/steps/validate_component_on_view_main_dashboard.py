from behave import given, when, then
from features.framework.environment import cph_ivt_app
import time

@then('I see a find county button with a link "{component}"')
def step_implement(context, component):
	cph_ivt_app.verify_component_exists(component)

@then('I see a find sate button with a link "{component}"')
def step_implement(context, component):
	cph_ivt_app.verify_component_exists(component)

# Verify the find a location is on the
# Main dashboard when a user first
# Accesses the CPH-IVT web application
@then(u'I see content "{component}"')
def setp_implement(context, component):
	cph_ivt_app.verify_component_exists(component)

@then(u'I exit the webapp')
def setu_implement(context):
	cph_ivt_app.teardown()


