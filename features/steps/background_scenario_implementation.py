from behave import given, when, then
from features.framework.environment import cph_ivt_app
import time

@given('I am on public user home page')
def step_implement(context):
	cph_ivt_app.load_website()

@when(u'I go to the "{page}" page')
def setp_implement(context, page):
	cph_ivt_app.goto_page(page)
	time.sleep(2)