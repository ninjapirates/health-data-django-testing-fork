from behave import given, when, then
from features.framework.environment import cph_ivt_app
import time

@then('I enter "{component}" in the search field')
def step_implement(context, component):

	element_id = "search_box"

	# Locate the element with the specified id
	element = cph_ivt_app.input_text_to_form(element_id)

	# Type text in the field
	element.send_keys(component)
	time.sleep(2)  # Wait for two seconds


@then(u'I click the search button')
def step_implement(context):
	search_btn = '//*[@id="menuDropdown"]/form/button'

	# click on the search button
	cph_ivt_app.click_component_by_xpath(search_btn)

# Search for a county using the search feature
@then('I get a page that display "{component}" as a county in Tennessee')
def step_implement(context, component):
	cph_ivt_app.verify_component_exists(component)

# Search for a state using the search feature
@then('I get a page that display "{component}" as a state')
def step_implement(context, component):
	cph_ivt_app.verify_component_exists(component)