from behave import given, when, then
from features.framework.environment import cph_ivt_app
import time

@then(u'I click on manage data')
def step_implement(context):
   manage_data = '//*[@id="bs-example-navbar-collapse-1"]/ul/li[2]/a'
   cph_ivt_app.click_component_by_xpath(manage_data)
   time.sleep(2)

@then('I validate the current link is "{link}"')
def step_implement(context, link):
   assert cph_ivt_app.find_the_current_url() == link


@then(u'I click on manage user')
def step_implement(context):
    manage_user = '//*[@id="bs-example-navbar-collapse-1"]/ul/li[3]/a'
    cph_ivt_app.click_component_by_xpath(manage_user)
    time.sleep(2)


@then(u'I click on public site')
def step_implement(context):
    public_site = '//*[@id="bs-example-navbar-collapse-1"]/ul/li[4]/a'
    cph_ivt_app.click_component_by_xpath(public_site)
    time.sleep(2)

@then(u'I click on CPH')
def step_implement(context):
    etsu_cph = '//*[@id="bs-example-navbar-collapse-1"]/ul/li[5]/a'
    cph_ivt_app.click_component_by_xpath(etsu_cph)
    time.sleep(2)


@then(u'I click on sign out')
def step_implement(context):
    etsu_cph = '//*[@id="bs-example-navbar-collapse-1"]/p/a'
    cph_ivt_app.click_component_by_xpath(etsu_cph)
    time.sleep(2)

