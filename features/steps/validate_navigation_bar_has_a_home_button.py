from behave import given, when, then
from features.framework.environment import cph_ivt_app
import time

@then('I click on the home button')
def set_implement(context):
	home_btn = '//*[@id="menuDropdown"]/ul/li/a/i'
	# click on the home button
	cph_ivt_app.click_component_by_xpath(home_btn)


@then('I confirm that I am still on the home page')
def set_implement(context):
	current_url = 'http://localhost:8000/'
	print(current_url)
	assert cph_ivt_app.find_the_current_url() == current_url
	time.sleep(2)
