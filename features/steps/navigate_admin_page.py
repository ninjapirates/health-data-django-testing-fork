from behave import given, when, then
from features.framework.environment import cph_ivt_app
import time

@then(u'I click on the user icon at the bottom right of the footer')
def step_implement(context):
   element_id = "/html/body/footer/div/a"
   cph_ivt_app.click_component_by_xpath(element_id)
   time.sleep(2)

@then('I validate that the current link is "{link}"')
def step_implement(context, link):
   assert cph_ivt_app.find_the_current_url() == link

@then(u'I enter "{component}" as my username')
def step_implement(context, component):
   element = "id_username"
   element_u = cph_ivt_app.input_text_to_form(element)
   element_u.send_keys(component)
   time.sleep(2)

@then(u'I enter "{component}" as my password')
def step_implement(context, component):
   element   = "id_password"
   element_p = cph_ivt_app.input_text_to_form(element)
   element_p.send_keys(component)
   time.sleep(2)

@then(u'I click on the login')
def step_implement(context):
   submit_btn = '//*[@id="submit"]'
   cph_ivt_app.click_component_by_xpath(submit_btn)
   time.sleep(2)

@then(u'I click on the cancel button')
def step_implement(context):
	cancel_btn = '// *[ @ id = "loginForm"] / footer / a'
	cph_ivt_app.click_component_by_xpath(cancel_btn)
