Feature: Admin Page Navigation Bar Validation
  In order to carry out my management task faster
  As a user of CPH-IVT
  I want to be able to use quick access in the privileged user dashboard navigation bar

  Background: I am on the login page
    Given I am on the login page
    When I go to the login page "http://localhost:8000/priv/login/"
    Then I enter "admin" as my username
    Then I enter "testing" as my password
    Then I click on the login


# Scenario: Validates that admin page log in works
#            When user clicks button to submit
#           redirects user to privileged user main page

  Scenario: Confirm manage data link works
    Then I click on manage data
    Then I validate the current link is "http://localhost:8000/priv/home/"


  Scenario: Confirm manage user link works
    Then I click on manage user
    Then I validate the current link is "http://localhost:8000/priv/usermanagement"


  Scenario: Confirm public site link works
    Then I click on public site
    Then I validate the current link is "http://localhost:8000/"


  Scenario: Confirm CPH link works
    Then I click on CPH
    Then I validate the current link is "http://localhost:8000/"

  Scenario: Confirm sign out link works
    Then I click on sign out
    Then I validate the current link is "http://localhost:8000/priv/login/"
    And I exit the webapp






