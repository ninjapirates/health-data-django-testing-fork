## Automated UI Test Environment Configuration ##
Ensure that all the prerequisites are met before beginning the testing process. 
1. Check the version of python being used for the project  
2. Install all the python required files. 
3. Setup the work environment by following instructions from: 
    `https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/`

For automated UI testing, the web-application is tested using `Selenium` and `behave-django`.
To setup the environment follow the steps below: 
1. `pip install selenium`
2. `pip install behave-django`
3. In the django settings file, add to INSTALLED_APPS= [`behave_django`]
4. Add a webdriver either `chromedriver.exe` or `geckodrver.exe` in the same location as manage.py
5. Running the tests use the command `python manage.py behave`